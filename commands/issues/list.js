const dotenv = require('dotenv');
dotenv.config();

const { SlashCommandBuilder } = require('discord.js');
const { getProjectIssues } = require('../../gitlab.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('list')
		.setDescription('Lists issues in default project'),
	async execute(interaction) {
        let message = await getProjectIssues(process.env.GITLAB_PROJECT_ID, 5);
        message.ephemeral = true;
        await interaction.reply(message);
		//await interaction.reply({content: projects,  ephemeral: true } );
	},
};
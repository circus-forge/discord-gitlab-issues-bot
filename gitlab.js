const dotenv = require('dotenv');
dotenv.config();
//import { Gitlab } from '@gitbeaker/rest';

const Gitlab = require('@gitbeaker/rest');

const api = new Gitlab.Gitlab({
  token: process.env.GITLAB_TOKEN,
});

async function getProjectIssues(projectId, qty)
{

   let issues = await api.Issues.all({
    projectId: projectId,
    maxPages: 1, 
    perPage: qty
   });
 
   let project = await api.Projects.show(projectId);
   
   let projectIssueString = qty + " most recent issues:\n";
   for (let i = 0; i < issues.length; i++) {
    projectIssueString += issues[i].title + "\n";
   }

   let message = {
    "content": projectIssueString,
    "components": [
        {
            "type": 1,
            "components": [
                {
                    "type": 2,
                    "label": "View on GitLab",
                    "style": 5,
                    "url": project._links.issues
                }
            ]

        }
    ]
};
    return message;


}

module.exports = { getProjectIssues }